/*!
  * @file     base_timer_port.c
  *
  * @brief    系统计时器用户接口
  *
  * @company   
  *
  * @author   不咸不要钱
  *
  * @note     
  *
  * @version  V1.2
  *
  * @Software C99
  *
  * @par URL  
  *           
  *
  * @date     2019/07/15
  */ 
#include "base_timer.h"


/*!
  * @brief    初始化定时器
  *
  * @param    clock:  定时器时钟
  * @param    ticks:  定时器每s中断ticks次
  *
  * @return   无
  *
  * @note     移植时需要根据使用的定时器实现该函数
  *
  * @see      btimer_port_init(72000000, 1000);  
  *
  * @date     2019/07/15
  */
void btimer_port_init(uint32_t clock, uint16_t ticks)
{

}

/*!
  * @brief    关闭定时器
  *
  * @param    无
  *
  * @return   无
  *
  * @note     
  *
  * @see      btimer_port_deinit();  
  *
  * @date     2019/07/15
  */
void btimer_port_deinit(void)
{

}

/*!
  * @brief    获取定时器的tick值
  *
  * @param    无
  *
  * @return   ticks
  *
  * @note     移植时需要根据使用的定时器实现该函数
  *
  * @see      
  *
  * @date     2019/07/15
  */
uint32_t btimer_port_get_ticks(void)
{
    return ;
}


#if defined(SOFT_TIMER_ENABLE) && (SOFT_TIMER_ENABLE == 1)
/*!
  * @brief    加锁
  *
  * @param    
  *
  * @return   无
  *
  * @note     
  *
  * @see      
  */
void stimer_port_lock(void)
{

}

/*!
  * @brief    开锁
  *
  * @param    无
  *
  * @return   无
  *
  * @note     
  *
  * @see      
  */
void stimer_port_unlock(void)
{

}

#endif



