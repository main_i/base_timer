/*!
  * @file     base_timer.h
  *
  * @brief    系统计时器
  *
  * @company   
  *
  * @author   不咸不要钱
  *
  * @note     
  *
  * @version  V1.2
  *
  * @Software C99
  *
  * @par URL  
  *           
  *
  * @date     2019/07/15
  */ 
#ifndef __BASE_TIMER_H
#define __BASE_TIMER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

/* 用户函数 */
void btimer_init(uint32_t clock, uint16_t ticks);
void btimer_deinit(void);
uint64_t btimer_get_ticks(void);
uint32_t btimer_get_us(void);
uint32_t btimer_get_ms(void);
void btimer_delay_ticks(uint32_t ticks);
void btimer_delay_us(uint32_t us);
void btimer_delay_ms(uint32_t ms);
void btimer_ticks(void);


/*! 是否使能软件定时器 */
#define SOFT_TIMER_ENABLE  1

#if defined(SOFT_TIMER_ENABLE) && (SOFT_TIMER_ENABLE == 1)


/*! 定时器重复最大次数 */
#define COUNTER_MAX  (0xFF)

/*! 函数指针 指向软件定时器超时回调函数 */
typedef void (*stimerCallback_t)(void *);

/** 
  * @brief stimer 软件定时器结构体
  * @note  无
  */
typedef struct Stimer
{
    volatile uint8_t  ucCounter;
    volatile uint8_t  ucFlag;
    volatile uint32_t ulTicks;
             uint32_t ulPerLoad;
    stimerCallback_t  stimerCallback;
    struct Stimer * ptNextTimer;
}Stimer_t;

void stimer_init(Stimer_t * stimer, uint16_t ticks, uint8_t count, stimerCallback_t cb);
void stimer_start(Stimer_t * stimer);
void stimer_stop(Stimer_t * stimer);
uint8_t stimer_is_timeout(Stimer_t * stimer);

void stimer_port_lock(void);
void stimer_port_unlock(void);
#endif




/* 移植时需用户实现 */
void btimer_port_init(uint32_t clock, uint16_t ticks);
void btimer_port_deinit(void);
uint32_t btimer_port_get_ticks(void);





#ifdef __cplusplus
}
#endif

#endif
