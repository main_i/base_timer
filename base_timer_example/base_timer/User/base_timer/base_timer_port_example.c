/*!
  * @file     base_timer_port.c
  *
  * @brief    系统计时器用户接口
  *
  * @company   
  *
  * @author   不咸不要钱
  *
  * @note     
  *
  * @version  V1.2
  *
  * @Software C99
  *
  * @par URL  
  *           
  *
  * @date     2019/07/15
  */ 
#include "base_timer.h"


#include "stm32f1xx_hal.h"

/* 使用freertos  利用互斥信号量进行线程保护 */
//#define USE_FREERTOS

#if defined(USE_FREERTOS)
#include "FreeRTOS.h"
#include "semphr.h"
static SemaphoreHandle_t s_timerSemaphoreHandle;
#endif

/*!
  * @brief    初始化定时器
  *
  * @param    clock:  定时器时钟
  * @param    ticks:  定时器每s中断ticks次
  *
  * @return   无
  *
  * @note     移植时需要根据使用的定时器实现该函数
  *
  * @see      btimer_port_init(72000000, 1000);  
  *
  * @date     2019/07/15
  */
void btimer_port_init(uint32_t clock, uint16_t ticks)
{
    /* 开启systick 定时器 */
    SysTick_Config(clock / ticks); 

    #if defined(USE_FREERTOS)
        s_timerSemaphoreHandle = xSemaphoreCreateBinary();
        xSemaphoreGive(s_timerSemaphoreHandle);
    #endif
}

/*!
  * @brief    关闭定时器
  *
  * @param    无
  *
  * @return   无
  *
  * @note     
  *
  * @see      btimer_port_deinit();  
  *
  * @date     2019/07/15
  */
void btimer_port_deinit(void)
{
    SysTick->CTRL &= ~(SysTick_CTRL_CLKSOURCE_Msk |
                       SysTick_CTRL_TICKINT_Msk |
                       SysTick_CTRL_ENABLE_Msk);
    SysTick->VAL = 0;
}

/*!
  * @brief    获取定时器的tick值
  *
  * @param    无
  *
  * @return   ticks
  *
  * @note     移植时需要根据使用的定时器实现该函数
  *
  * @see      
  *
  * @date     2019/07/15
  */
uint32_t btimer_port_get_ticks(void)
{
    return (uint32_t)SysTick->LOAD - (uint32_t)SysTick->VAL;
}


#if defined(SOFT_TIMER_ENABLE) && (SOFT_TIMER_ENABLE == 1)
/*!
  * @brief    加锁
  *
  * @param    
  *
  * @return   无
  *
  * @note     
  *
  * @see      
  */
void stimer_port_lock(void)
{
    #if defined(USE_FREERTOS)
        xSemaphoreTake(s_timerSemaphoreHandle, portMAX_DELAY);
    #else
        __set_PRIMASK(1);    /* 禁止全局中断 */
    #endif
}

/*!
  * @brief    开锁
  *
  * @param    无
  *
  * @return   无
  *
  * @note     
  *
  * @see      
  */
void stimer_port_unlock(void)
{
    #if defined(USE_FREERTOS)
        xSemaphoreGive(s_timerSemaphoreHandle);
    #else
        __set_PRIMASK(0);    /* 使能全局中断 */
    #endif
}

#endif

/*!
  * @brief    systick 中断服务函数
  *
  * @param    无
  *
  * @return   无
  *
  * @note     移植时需要在定时器中断服务函数中调用 btimer_ticks(); 
  *
  * @see      无
  *
  * @date     2019/07/15
  */
__weak void SysTick_Handler(void)
{
	  btimer_ticks();
}

