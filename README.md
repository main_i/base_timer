# base_timer

## 介绍
单片机裸机适用的定时器小组件,通过一个定时器记录系统运行时长，为用户提供一个时间戳信息，当然也有软件定时器功能。


## 移植教程

1.  将 base_timer.c base_timer.h base_timer_port.c 添加到工程中
2.  打开 base_timer_port.c 文件，自行实现里面的函数, 其中最重要的是 btimer_port_init(uint32_t clock, uint16_t ticks)函数和 uint32_t btimer_port_get_ticks(void)函数 
```c
/*!
  * @brief    初始化定时器
  *
  * @param    clock:  定时器时钟
  * @param    ticks:  定时器每s中断ticks次
  *
  * @return   无
  *
  * @note     移植时需要根据使用的定时器实现该函数
  *
  * @see      btimer_port_init(72000000, 1000);  
  *
  * @date     2019/07/15
  */
void btimer_port_init(uint32_t clock, uint16_t ticks)
{

}

/*!
  * @brief    获取定时器的tick值
  *
  * @param    无
  *
  * @return   ticks
  *
  * @note     移植时需要根据使用的定时器实现该函数
  *
  * @see      
  *
  * @date     2019/07/15
  */
uint32_t btimer_port_get_ticks(void)
{
    return ;
}
```
3.  将 btimer_ticks();函数添加到定时器中断服务函数中，周期性的执行。

## 使用说明

1.  初始化定时器
```c
void btimer_init(uint32_t clock, uint16_t ticks);
```
2.  定时器初始化后，系统内就存在了一个时基，可以获取系统从定时器初始化到现在的运行 ticks，通过这个时间戳信息，就可以实现各种各样的功能了。
需要注意的是，us和ms相关的函数都是在调用 uint64_t btimer_get_ticks(void); 函数的基础上使用除法实现的，但是没有double FPU的单片机对 uint64_t 的除法运算比较耗时，实测STM32F103 72MHz时， 一次uint64_t 需要50us左右
```c
uint64_t btimer_get_ticks(void);
uint32_t btimer_get_us(void);
uint32_t btimer_get_ms(void);
void btimer_delay_ticks(uint32_t ticks);
void btimer_delay_us(uint32_t us);
void btimer_delay_ms(uint32_t ms);
```
3.  组件内同样也有实现软件定时器，在 base_timer.h 中使能
```c
/*! 是否使能软件定时器 */
#define SOFT_TIMER_ENABLE  1
```
* 调用软件定时器初始化函数初始化一个软件定时器并编写超时回调函数
```c
/* 定时器超时回调函数 */
void stimerLed1Callback(void * parame)
{
  //do something
}

void stimer_init(Stimer_t * stimer, uint16_t ticks, uint8_t count, stimerCallback_t cb);
```

* 开启关闭定时器
```c 
void stimer_start(Stimer_t * stimer);
void stimer_stop(Stimer_t * stimer);
```

## 使用例子

```c

Stimer_t g_stimerLed1, g_stimerLed2, g_stimerLed3;

/* 定时器超时回调函数 */
void stimerLed1Callback(void * parame)
{
  //do something
}

void stimerLed2Callback(void * parame)
{
  //do something
}

void stimerLed3Callback(void * parame)
{
  //do something
}

int main(void)
{

  /* 初始化定时器 定时器时钟 72MHz 每秒中断1000次 */
  btimer_init(72000000, 1000);
  
  /* 获取时间戳 */
  uint64_t ticks1 = btimer_get_ticks();
  
  /* 延时 10000us */
  btimer_delay_us(10000);
  
  /* 获取时间戳 通过 ticks2 - ticks1 可以获得 btimer_delay_us(10000); 运行时长 */
  uint64_t ticks2 = btimer_get_ticks();
  
  /* 延时 10ms */
  btimer_delay_ms(10);
  
  stimer_init(&g_stimerLed1, 100,  COUNTER_MAX, stimerLed1Callback);
  stimer_init(&g_stimerLed2, 1000, 1,           stimerLed2Callback);
  stimer_init(&g_stimerLed3, 1000, COUNTER_MAX, stimerLed3Callback);
  
  stimer_start(&g_stimerLed1);
  stimer_start(&g_stimerLed2);
  stimer_start(&g_stimerLed3);

  while (1)
  {
    if(stimer_is_timeout(&g_stimerLed2))
    {
      stimer_init(&g_stimerLed2, 1000, 1,           stimerLed2Callback);
      stimer_start(&g_stimerLed2);
    }
  }
}

```




